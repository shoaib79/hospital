<x-app-layout>
    <div class="px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto">

        <!-- Welcome banner -->
        <x-dashboard.welcome-banner />

        <!-- Dashboard actions -->
        <div class="sm:flex sm:justify-between sm:items-center mb-8">

            <!-- Right: Actions -->
            <div class="grid grid-flow-col sm:auto-cols-max justify-start sm:justify-end gap-2">

                <!-- Add view button -->
                <a href="{{ route('campaigns.create') }}"
                   class="bg-green-700 hover:bg-red-700 text-white py-2 px-4 rounded">
                    <span class="hidden xs:block ml-2">Add Campaign</span>
                </a>


            </div>

        </div>

        <table class="w-full border-collapse border">
            <thead>
            <tr>
                <th class="p-4 bg-gray-100 border border-gray-300">ID</th>
                <th class="p-4 bg-gray-100 border border-gray-300">Title</th>
                <th class="p-4 bg-gray-100 border border-gray-300">Description</th>
                <th class="p-4 bg-gray-100 border border-gray-300">Date & Time</th>
                <th class="p-4 bg-gray-100 border border-gray-300">Address</th>
                <th class="p-4 bg-gray-100 border border-gray-300">City</th>
                <th class="p-4 bg-gray-100 border border-gray-300">Actions</th>
                <!-- Other columns -->
            </tr>
            </thead>
            <tbody>
            @foreach($campaigns as $campaign)
                <tr class="bg-white hover:bg-gray-100">
                    <td class="p-4 border border-gray-300">{{ $loop->index+1 }}</td>
                    <td class="p-4 border border-gray-300">{{ $campaign->title }}</td>
                    <td class="p-4 border border-gray-300">{{ $campaign->description }}</td>
                    <td class="p-4 border border-gray-300">{{ $campaign->datetime }}</td>
                    <td class="p-4 border border-gray-300">{{ $campaign->address }}</td>
                    <td class="p-4 border border-gray-300">{{ $campaign->city }}</td>
                    <td class="p-4 border border-gray-300">
                        <a href="{{ route('campaigns.edit', $campaign->id) }}" class="bg-blue-400 hover:bg-blue-600 text-white  py-2 px-4 rounded">Edit</a>
                        <form action="{{ route('campaigns.destroy', $campaign->id) }}" method="POST" class="d-inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="bg-red-500 hover:bg-red-600 text-white py-2 px-4 rounded">Delete</button>
                        </form>
                    </td>
                    <!-- Other cells -->
                </tr>
            @endforeach
            </tbody>
        </table>

    {{ $campaigns->links() }} <!-- Display pagination links -->


    </div>
</x-app-layout>
