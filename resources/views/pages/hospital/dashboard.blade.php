<x-app-layout>
    <div class="px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto">

        <!-- Welcome banner -->
        <x-dashboard.welcome-banner />

        <!-- Dashboard actions -->
        <div class="sm:flex sm:justify-between sm:items-center mb-8">

        </div>

            <!-- Right: Actions -->
            <div class="grid grid-flow-col sm:auto-cols-max justify-start sm:justify-end gap-2">
            <h2 class="text-2xl md:text-3xl text-slate-800 dark:text-slate-100 font-bold mb-1">Patients List</h2>
                <!-- Add view button -->
                {{--<a href="{{ route('patient.create') }}"
                   class="bg-green-700 hover:bg-red-700 text-white py-2 px-4 rounded">
                    <span class="hidden xs:block ml-2">Add Patients</span>
                </a>--}}


            </div>

        </div>

    <div class="mb-3 ml-4">
        <label for="search" class="block text-sm font-medium text-gray-700">Search:</label>
        <input type="text" id="search" class="mt-1 p-2  border border-gray-300 rounded-md text-sm">
    </div>

    <table id="patient-table"  class="w-full border-collapse border table" data-searchable="true">
            <thead>
            <tr>
                <th class="p-4 bg-gray-100 border border-gray-300">ID</th>
                <th class="p-4 bg-gray-100 border border-gray-300">Name</th>
                <th class="p-4 bg-gray-100 border border-gray-300">Clinic</th>
                <th class="p-4 bg-gray-100 border border-gray-300">Mobile</th>
                <th class="p-4 bg-gray-100 border border-gray-300">Description</th>
                <th class="p-4 bg-gray-100 border border-gray-300">Location (Lat)(Lng)</th>
                <th class="p-4 bg-gray-100 border border-gray-300">Date & Time</th>
                <th class="p-4 bg-gray-100 border border-gray-300">Action</th>
                <!-- Other columns -->
            </tr>
            </thead>
            <tbody>
            @foreach($patients as $patient)
                <tr class="bg-white hover:bg-gray-100">
                    <td class="p-4 border border-gray-300">{{ $loop->index+1 }}</td>
                    <td class="p-4 border border-gray-300">{{ $patient->name }}</td>
                    <td class="p-4 border border-gray-300">{{ $patient->clinic }}</td>
                    <td class="p-4 border border-gray-300">{{ $patient->mobile }}</td>
                    <td class="p-4 border border-gray-300">{{ $patient->description }}</td>
                    <td class="p-4 border border-gray-300">{{ ($patient->lat)}} {{($patient->lng) }}</td>
                    <td class="p-4 border border-gray-300">{{ $patient->datetime }}</td>
                    <td class="p-4 border border-gray-300">
                       {{-- <a href="{{ route('patients.edit', ['patient' => $patient->id]) }}"
                           class="bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 rounded">
                            Edit
                        </a>--}}
                        <form method="POST" action="{{ route('patients.destroy', $patient->id) }}" class="inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="text-red-500 hover:text-red-700 focus:outline-none btn">
                                Delete
                            </button>
                        </form>

                    </td>
                    <!-- Other cells -->
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $patients->links() }} <!-- Display pagination links -->

    @push('script')
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.datatables.net/1.11.6/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.11.6/js/dataTables.bootstrap4.min.js"></script>

        <script>
            $(document).ready(function () {
                var table = $('#patient-table').DataTable({
                    "paging": true,
                    "ordering": true,
                    "searching": true, // Enable search functionality
                    "language": {
                        "search": "Search:",
                        "searchPlaceholder": "Enter search term"
                    }
                });

                $('#search').on('keyup', function () {
                    table.search(this.value).draw(); // Apply the search filter to the DataTable
                });
            });
        </script>

        <script>
            $(document).ready(function () {
                var table = $('#patient-table').DataTable({
                    "paging": true,
                    "ordering": true,
                    "searching": true, // Enable search functionality
                    "language": {
                        "search": "Search:",
                        "searchPlaceholder": "Enter search term"
                    }
                });

                $('#search').on('keyup', function () {
                    table.search(this.value).draw(); // Apply the search filter to the DataTable
                });
            });
        </script>

        @endpush

    </div>



</x-app-layout>

