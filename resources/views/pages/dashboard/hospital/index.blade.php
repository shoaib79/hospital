<x-app-layout>
    <div class="px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto">
        <div class="max-w-5xl mx-auto mt-8">
            <h1 class="text-2xl font-semibold mb-4">Hospital List</h1>
            <table class="w-full border">
                <thead>
                <tr class="bg-gray-100">
                    <th class="border px-4 py-2">#</th>
                    <th class="border px-4 py-2">Name</th>
                    <th class="border px-4 py-2">Contact</th>
                    <th class="border px-4 py-2">Email</th>
                    <th class="border px-4 py-2">Address</th>
                    <th class="border px-4 py-2">City</th>
                    <th class="border px-4 py-2">Status</th>
                    <th class="border px-4 py-2">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($hospitals as $hospital)
                    <tr>
                        <td class="border px-4 py-2">{{ $hospital->id }}</td>
                        <td class="border px-4 py-2">{{ $hospital->name }}</td>
                        <td class="border px-4 py-2">{{ $hospital->contact_number }}</td>
                        <td class="border px-4 py-2">{{ $hospital->email }}</td>
                        <td class="border px-4 py-2">{{ $hospital->address }}</td>
                        <td class="border px-4 py-2">{{ $hospital->city }}</td>
                        <td class="border px-4 py-2">
                            @if ($hospital->is_approved)
                                <span class="text-green-500">Approved</span>
                            @else
                                <span class="text-red-500">Not Approved</span>
                            @endif
                        </td>

                        <td class="border px-4 py-2">
                            <a href="{{ route('hospitals.show', $hospital->id) }}" class="btn btn-primary bg-amber-500">View</a>
                            <form action="{{ $hospital->is_approved ? route('hospitals.disapprove', $hospital) : route('hospitals.approve', $hospital) }}" method="POST" class="inline">
                                @csrf
                                @method('PUT')
                                <button type="submit" class="bg-{{ $hospital->is_approved ? 'red' : 'blue' }}-500 hover:bg-{{ $hospital->is_approved ? 'red' : 'blue' }}-700 text-white font-bold py-2 px-4 rounded focus:outline-none">
                                    @if ($hospital->is_approved)
                                        Disapprove
                                    @else
                                        Approve
                                    @endif
                                </button>
                            </form>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        {{ $hospitals->links() }} <!-- Display pagination links -->

        </div>
    </div>
</x-app-layout>
