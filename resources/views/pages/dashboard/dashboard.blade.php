<x-app-layout>
    <div class="px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto">

        <!-- Welcome banner -->
        <x-dashboard.welcome-banner />

        <!-- Dashboard actions -->
        <div class="sm:flex sm:justify-between sm:items-center mb-8">

            <!-- Right: Actions -->
            <div class="grid grid-flow-col sm:auto-cols-max justify-start sm:justify-end gap-2">

                <!-- Add view button -->
                <a href="{{ route('services.create') }}"
                   class="bg-green-700 hover:bg-red-700 text-white py-2 px-4 rounded">
                    <span class="hidden xs:block ml-2">Add Service</span>
                </a>


            </div>

        </div>


            <table  class="w-full border-collapse border table" data-searchable="true">
                <thead>
                <tr>
                    <th class="p-4 bg-gray-100 border border-gray-300">ID</th>
                    <th class="p-4 bg-gray-100 border border-gray-300">Name</th>
                    <th class="p-4 bg-gray-100 border border-gray-300">Status</th>
                    <th class="p-4 bg-gray-100 border border-gray-300">Action</th>
                    <!-- Other columns -->
                </tr>
                </thead>
                <tbody>
                @foreach($services as $service)
                    <tr class="bg-white hover:bg-gray-100">
                        <td class="p-4 border border-gray-300">{{ $loop->index+1 }}</td>
                        <td class="p-4 border border-gray-300">{{ $service->name }}</td>
                        <td class="p-4 border border-gray-300">{{ $service->status }}</td>
                        <td class="p-4 border border-gray-300">
                            <a href="{{ route('services.edit', ['service' => $service->id]) }}"
                               class="bg-blue-400 hover:bg-blue-700 text-white py-2 px-4 rounded">
                                Edit
                            </a>
                            <form method="POST" action="{{ route('services.destroy', $service->id) }}" class="inline">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="text-red-500 hover:text-red-700 focus:outline-none btn">
                                    Delete
                                </button>
                            </form>

                        </td>
                        <!-- Other cells -->
                    </tr>
                @endforeach
                </tbody>
            </table>

        {{ $services->links() }} <!-- Display pagination links -->



    </div>
</x-app-layout>
