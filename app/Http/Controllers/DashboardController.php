<?php

    namespace App\Http\Controllers;

    use App\Models\Service;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Http;
    use App\Models\DataFeed;
    use Carbon\Carbon;

    class DashboardController extends Controller
    {

        /**
         * Displays the dashboard screen
         *
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
         */
        public function index()
        {
            $services = Service::paginate(10);

            return view('pages.dashboard.dashboard', compact('services'));

           // return view('pages/dashboard/dashboard', compact('dataFeed'));
        }
        public function show($id)
        {
            // Display the details of a specific service
        }
        public function create()
        {
            return view('pages.dashboard.services.create');
        }

        public function store(Request $request)
        {
            $validatedData = $request->validate([
                'name' => 'required|string|max:255',
                'status' => 'required|in:Available,Busy',
            ]);

            Service::create($validatedData);

            return redirect()->route('services.index')->with('success', 'Service created successfully');
        }

        public function edit($id)
        {
            $service = Service::findOrFail($id);
            return view('pages.dashboard.services.edit', compact('service'));
        }

        public function update(Request $request, $id)
        {
            $service = Service::findOrFail($id);
            $service->update($request->all());
            return redirect()->route('services.index')->with('success', 'Service updated successfully');
        }

        public function destroy($id)
        {

            $service = Service::findOrFail($id);
            $service->delete();
            return $this->index();
        }
    }
