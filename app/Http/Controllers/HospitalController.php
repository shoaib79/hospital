<?php

namespace App\Http\Controllers;

use App\Models\Hospital;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HospitalController extends Controller
{

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'contact_number' => 'required|string|max:255',
            'email' => 'required|email|unique:hospitals',
            'address' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'password' => 'required|string|min:8|confirmed',
        ]);

        Hospital::create([
            'name' => $request->name,
            'contact_number' => $request->contact_number,
            'email' => $request->email,
            'address' => $request->address,
            'city' => $request->city,
            'password' => Hash::make($request->password),
        ]);

        return redirect()->route('login')->with('success', 'Hospital registered successfully! You can now log in.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hospitals = Hospital::paginate(10);
        return view('pages.dashboard.hospital.index', compact('hospitals'));
    }

    public function create()
    {
        return view('pages.dashboard.hospital.create');
    }

    public function store(Request $request)
    {
        // Validation and store logic
    }

    public function edit($id)
    {
        $hospital = Hospital::findOrFail($id);
        return view('pages.dashboard.hospital.edit', compact('hospital'));
    }

    public function update(Request $request, $id)
    {
        $hospital = Hospital::findOrFail($id);

        // Validation and update logic
        $hospital->update($request->all());

        return redirect()->route('hospitals.index')->with('success', 'Hospital updated successfully');
    }

    public function destroy($id)
    {
        // Delete logic
    }

    public function approve(Hospital $hospital)
    {
        $hospital->update(['is_approved' => true]);
        return    $this->index();
    }

    public function disapprove(Hospital $hospital)
    {
        $hospital->update(['is_approved' => false]);
        return redirect()->route('hospitals.index')->with('success', 'Hospital disapproved successfully');
    }

    public function show($id)
    {
        $hospital = Hospital::findOrFail($id);
        return view('pages.dashboard.hospital.show', compact('hospital'));
    }

    public function editPassword()
    {
        return view('pages.hospital.update-password');
    }

    public function updatePassword(Request $request)
    {

        $user = auth()->guard('hospital')->user();

        if (Hash::check($request->current_password, $user->password)) {
            $user->update([
                'password' => Hash::make($request->password),
            ]);

            return redirect()->route('hospital.editPassword')->with('success', 'Password updated successfully.');
        } else {
            return redirect()->route('hospital.editPassword')->with('error', 'Current password is incorrect.');
        }
    }

}
