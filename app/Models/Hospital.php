<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Hospital  extends Authenticatable

{
    use HasFactory;

    protected $fillable = [
        'name',
        'contact_number',
        'email',
        'address',
        'city',
        'password',
        'is_approved', // Add this line
    ];

    public function services()
    {
        return $this->belongsToMany(Service::class)
            ->withPivot('status')
            ->withTimestamps();


    }

}
