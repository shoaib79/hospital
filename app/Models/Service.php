<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = ['name', 'status'];
    use HasFactory;

    public function hospitals()
    {
        return $this->belongsToMany(Hospital::class)
            ->withPivot('status')
            ->withTimestamps();
    }

}
