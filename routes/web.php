<?php

use App\Http\Controllers\CampaignController;
use App\Http\Controllers\HospitalController;
use App\Http\Controllers\HospitalLoginController;
use App\Http\Controllers\HospitalServicesController;
use App\Http\Controllers\PatientController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DataFeedController;
use App\Http\Controllers\DashboardController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('register', [HospitalController::class, 'register']);

Route::get('login/hospital', [HospitalLoginController::class, 'showLoginForm'])->name('hospital.login');
Route::post('login/hospital', [HospitalLoginController::class, 'login']);



Route::redirect('/', 'login');

Route::middleware(['auth:sanctum', 'verified'])->group(function () {

    // Route for the getting the data feed
    Route::get('/json-data-feed', [DataFeedController::class, 'getDataFeed'])->name('json_data_feed');

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::resource('services',DashboardController::class);
    Route::resource('hospitals', HospitalController::class);
    Route::put('hospitals/approve/{hospital}', [HospitalController::class, 'approve'])->name('hospitals.approve');
    Route::put('hospitals/disapprove/{hospital}', [HospitalController::class, 'disapprove'])->name('hospitals.disapprove');

    Route::fallback(function() {
        return view('pages/utility/404');
    });


});


Route::middleware(['auth:hospital'])->group(function () {
    Route::resource('patients', PatientController::class);
    Route::resource('campaigns', CampaignController::class);
    Route::resource('hospital-services',\App\Http\Controllers\HospitalServicesController::class);

    Route::get('hospital-services/{hospital}/update-view', [HospitalServicesController::class, 'updateServicesView'])
        ->name('hospital-services.update-view');

   /* Route::put('hospital-services/{hospital}', [HospitalServicesController::class, 'updateServices'])
        ->name('hospital-services.update');*/

    Route::get('/hospital/update-password', [HospitalController::class,'editPassword'])->name('hospital.editPassword');
    Route::post('/hospital/update-password', [HospitalController::class,'updatePassword'])->name('hospital.updatePassword');




});
