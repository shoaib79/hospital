<?php

use App\Http\Controllers\Api\HospitalController;
use App\Http\Controllers\Api\PatientController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AmbulanceAuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});



Route::post('/ambulance/register', [AmbulanceAuthController::class, 'register']);
Route::post('/ambulance/login', [AmbulanceAuthController::class, 'login']);
Route::get('/hospitals', [HospitalController::class, 'index']);
Route::post('/dropPatientToHospital', [PatientController::class, 'dropPatientToHospital']);
